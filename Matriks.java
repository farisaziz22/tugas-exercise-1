import java.util.*;
import java.io.*;

public class Matriks
{
    public final int M;             // baris
    public final int N;             // kolom
    public final double[][] nilai;   // nilai array2dimensi
    // buat matiks kosong berisi 0
    public Matriks(int M, int N) {
      this.M=M;
      this.N=N;
      nilai = new double[M][N];
    }
    // buat matriks dari array 2dimensi
    public Matriks(double[][] nilai) {
      M = nilai.length;
      N = nilai[0].length;
      this.nilai = new double[M][N];
      for (int i=0;i<M;i++) {
        for (int j=0;j<N;j++) {
          this.nilai[i][j]= nilai[i][j];
        }
      }
    }


    //konstruktor
    private Matriks(Matriks A){
      this(A.nilai);
    }
    // tukar baris
    private void swap(int i, int j) {
      double[] temp=nilai[i];
      nilai[i]=nilai[j];
      nilai[j]=temp;
    }

    // Ganti Kolom
    public Matriks GantiKolom(Matriks B, int k) {
      Matriks A = this;
      for (int i=0;i<A.M;i++) {
        A.nilai[i][k]=B.nilai[i][0];
      }
      return A;
    }
    // membuat matriks identitas dengan ukuran NxN
    public static Matriks Identitas(int N) {
      Matriks I = new Matriks(N,N);
      for (int i=0;i<N;i++) {
        I.nilai[i][i]=1;
      }
      return I;
    }
    // Meng-augmentasi matriks A dengan b
    public Matriks Augment(Matriks B) {
      Matriks A=this;
      Matriks C = new Matriks(A.M, A.N+B.N);
      for (int i=0;i<A.M;i++) {
        for (int j=0;j<A.N;j++) {
          C.nilai[i][j]= A.nilai[i][j];
        }
      }
      for (int i=0;i<C.M;i++) {
        for (int j=A.N;j<C.N;j++) {
          C.nilai[i][j]= B.nilai[i][j-A.N];
        }
      }
      return C;
    }
    // Menghapus kolom terakhir
    public Matriks DeleteLastColumn() {
      Matriks A = this;
      Matriks C = new Matriks(A.M, A.N-1);
      for (int i=0;i<C.M;i++) {
        for (int j=0;j<C.N;j++) {
          C.nilai[i][j]=A.nilai[i][j];
        }
      }
      return C;
    }
    // Mendapatkan kolom terakhir
    public Matriks GetLastColumn() {
      Matriks A = this;
      Matriks C = new Matriks(A.M, 1);
      for (int i=0;i<C.M;i++) {
        C.nilai[i][0]=A.nilai[i][A.N-1];
      }
      return C;
    }
    // Transpose Matriks
    public Matriks Transpose() {
      Matriks A = this;
      Matriks trans = new Matriks(N,M);
      for (int i=0;i<A.M;i++) {
        for (int j=0;j<A.N;j++) {
          trans.nilai[j][i]= A.nilai[i][j];
        }
      }
      return trans;
    }
    // Matriks dikali konstanta
    public Matriks KaliKons(double kons) {
        Matriks A = this;
        for (int i=0;i<A.M;i++) {
          for (int j=0;j<A.N;j++) {
            A.nilai[i][j]*=kons;
          }
        }
        return A;
    }
    // Matriks A x B
    public Matriks Kali(Matriks B) {
      Matriks A = this;
      Matriks C = new Matriks(A.M, B.N);
      for (int i=0;i<C.M;i++) {
        for (int j=0;j<C.N;j++) {
          for (int k=0;k<A.N;k++) {
              C.nilai[i][j]+= (A.nilai[i][k]*B.nilai[k][j]);
          }
        }
      }
      return C;
    }
    // Baris i pada matriks dikali konstanta
    public Matriks KaliBaris(int IdxBrs, double kons) {
      Matriks A = this;
      int JmlKol = A.N;
      for (int i=0;i<JmlKol;i++) {
        A.nilai[IdxBrs][i]*=kons;
      }
      return A;
    }
    // Baris i pada matriks ditambah dengan baris j * konstanta
    public Matriks TambahBarisKons(int IdxBrs1, int IdxBrs2, double kons) {
      Matriks A=this;
      int JmlKol = A.N;

      for (int i=0;i<JmlKol;i++) {
        A.nilai[IdxBrs1][i]+=(A.nilai[IdxBrs2][i]*kons);
        A.nilai[IdxBrs1][i]+=+0.0;
      }
      return A;
    }
    // menuliskan matriks
    public void TulisMatriks() {
      for (int i=0;i<M;i++) {
        for (int j=0;j<N-1;j++) {
          System.out.printf("%9.4f ", nilai[i][j]);
        }
        System.out.printf("%9.4f", nilai[i][N-1]);
        System.out.println();
      }
    }

    //membaca input matriks
    public static Matriks BacaMatriks(int M, int N) {
      Scanner input = new Scanner(System.in);
      Matriks A=new Matriks(M,N);

      for(int i = 0; i < A.M; i++) {
        for (int j = 0; j < A.N; j++) {
          A.nilai[i][j] = input.nextDouble();
        }
      }
      return A;
    }
    // Mengopi matriks
    public Matriks Copy(Matriks B) {
      Matriks A = this;
      for (int i=0;i<A.M;i++) {
        for (int j=0;j<A.N;j++) {
          A.nilai[i][j] = B.nilai[i][j];
        }
      }
      return A;
    }

    // Penyulihan mundur solusi unik
    public static Matriks PenyulihanMundur(Matriks A) {
        // Jumlah baris dan kolom
        int jmlBrs=A.M;
        int jmlKol=A.N;
        // Inisilalisasi matriks input dan output
        Matriks Hasil=new Matriks(jmlBrs, 1);
  			for (int i = jmlBrs - 1; i >= 0; i--) {
  				double total = 0.0;
  				for (int j = i + 1; j < jmlBrs; j++) {
                  total += A.nilai[i][j] * Hasil.nilai[j][0];
  				}
  				Hasil.nilai[i][0] = (A.nilai[i][jmlKol-1] - total) / A.nilai[i][i];
			  }
        return Hasil;
  	}

    // Jenis solusi unik, banyak, tidak ada
    public static int JenisSolusi(Matriks A ) {
      int idx=-1;
      int cnt=0;
      int maksi=0;
      for (int i=0;i<A.N;i++) {
        if (A.nilai[A.M-1][i]!=0) {
          idx=i;
          break;
        }
      }
      if (idx>=0 && idx<=A.N-2) {
        int banyak= A.M-A.N+1;
        if (banyak<0) return 2;
        else return 1;
      }
      if (idx==A.N-1) return 0;
      if (idx==-1) return 2;
      return 2;
    }
    // determinan
    public double Determinan() {
      Matriks A = this;
      int utama=0;
      int k,p;
      double x;

      int JmlBaris = A.M;
      int JmlKolom = A.N;
      p=1;
      for (int i=0;i< JmlBaris-1;i++) {

        //mencari bilangan tak nol pertama
        k=i;
        while (k<JmlBaris && A.nilai[k][utama]==0) {
          k++;
        }
        if (JmlBaris==k) {
          utama++;
          continue;
        }

        if (k!=i) {
          A.swap(k,i);
          p*=-1;
        }

        // mengubah bilangan di bawah baris menjadi nol
        for (k=i+1;k<JmlBaris;k++) {
          x=-1*(A.nilai[k][utama]/A.nilai[i][utama]);
          A = A.TambahBarisKons(k, i, x);
        }
        utama++;
      }
      x=A.nilai[0][0];
      for (int i=1;i<A.M;i++) {
        x*=A.nilai[i][i];
      }
      return x*p;
    }
    // Matriks Kofaktor
    public Matriks Kofaktor(int a, int b, int N) {
      Matriks A=this;
      Matriks KoF=new Matriks(N,N);
      boolean masuk;
      int idxbrs,idxkolom;
      idxbrs=0;
      for (int i=0;i<A.M;i++) {
        idxkolom=0;
        masuk=false;
        for (int j=0;j<A.N;j++) {
          if (a==i || b==j) continue;
          KoF.nilai[idxbrs][idxkolom]=A.nilai[i][j];
          idxkolom++;
          masuk=true;
        }
        if (masuk) idxbrs++;
      }
      return KoF;
    }
    // Determinan menggunakan ekspansi kofaktor
    public double DetKof(int N) {
      Matriks A = this ;
      Matriks mino= new Matriks(N, N);
      double hasil=0;
      int idxmini, jmlmini, cnt;
      if (N==1) return A.nilai[0][0];
      if (N==2) return ((A.nilai[0][0]*A.nilai[1][1])-(A.nilai[1][0]*A.nilai[0][1]));
      mino=A.Minor();
      for (int i=0;i<A.N;i++) {
        hasil += A.nilai[0][i]*mino.nilai[0][i];
      }
      return hasil;

    }
    // MAtriks minor
    public Matriks Minor() {
      Matriks A=this;
      Matriks aminor = new Matriks(A.M, A.N);
      double x;
      for (int i=0;i<A.M;i++) {
        for (int j=0;j<A.N;j++) {
          aminor.nilai[i][j]= (A.Kofaktor(i,j,A.M-1)).Determinan();
          if ((i+j)%2!=0) aminor.nilai[i][j]*=-1;
        }
      }
      return aminor;
    }
    // Matriks inverse dengan metode kofaktor
    public Matriks InverseKof() {
      Matriks A=this;
      double Deter;
      Deter = A.Determinan();
      A=A.Minor();
      A=A.Transpose(); // Adjoin(A)
      return A.KaliKons(1/Deter);
    }
    // Matriks inverse dengan metode reduksi baris
    public Matriks Inverse() {
      Matriks A=this;
      Matriks I = new Matriks(Identitas(A.M));
      Matriks Aug = new Matriks(A.Augment(I));
      int Sekat=A.M;
      Aug = Aug.GaussJordan(Sekat);
      Matriks balikan = new Matriks(A.M, A.N);
      for (int i=0;i<Aug.M;i++) {
        for (int j=A.N;j<Aug.N;j++) {
          balikan.nilai[i][j-A.N]= Aug.nilai[i][j];
        }
      }
      return balikan;
    }

    // penyelesaian SPL Gauss
    public Matriks Gauss(int Sekat) {
      // sekat merupakan batas matriks augmented
      Matriks A = this;
      int k;
      double kons;
      // Jumlah  baris dan kolom matriks input
      int JmlBaris = A.M;
      int JmlKolom = A.N;
      int utama=0;

      for (int i=0;i<JmlBaris;i++) {
        if (JmlKolom<=utama) {
          break;
        }
        // mencari bilangan tak nol pertama
        k=i;
        while (A.nilai[k][utama]==0) {
          k++;
          if (JmlBaris==k) {
            k=i;
            utama++;
            if (JmlKolom-Sekat+1==utama) {
              utama-=1;
              break;
            }
          }
        }
        // Mengubahnya menjadi 1 utama
        swap(k,i);
        if (A.nilai[i][utama]!=0) {
          A = A.KaliBaris(i, (1.0/A.nilai[i][utama]));
        }
        // mengubah bilangan di bawah 1 utama dalam satu baris menjadi nextDouble
        for (k=i+1;k<JmlBaris;k++) {
          A = A.TambahBarisKons(k,i, ((-1.0)*A.nilai[k][utama]));
        }
        utama++;
      }
      // Antisipasi hasil -0.0
      for (int i=0;i<JmlBaris;i++) {
        for (int j=0;j<JmlKolom;j++) {
          A.nilai[i][j]+=+0.0;
        }
      }
      return A;
    }

    // penyelesaian SPL Gauss Jordan
    public Matriks GaussJordan(int Sekat) {
      Matriks A= this;
      int utama = 0;
      int k;

      // Jumlah baris dan kolom matriks input
      int jmlBrs = A.M;
      int jmlKol = A.N;

      for(int i = 0; i < jmlBrs; i++){
        if(jmlKol <= utama){
          break;
        }
        // Mencari bilangan tak nol pertama
        k = i;
        while(A.nilai[k][utama] == 0){
          k++;
          if(jmlBrs == k){
            k = i;
            utama++;
            if(jmlKol == utama){
              utama-=1;
              break;
            }
          }
        }
        // Mengubahnya menjadi 1 utama
        A.swap(k, i);
        if(A.nilai[i][utama] != 0){
          A = A.KaliBaris(i, (1/A.nilai[i][utama]));
        }
        // Mengubah bilangan selain 1 utama dalam satu baris menjadi nol
        for(k = 0; k < jmlBrs; k++){
          if(k != i){
            A = A.TambahBarisKons(k, i,((-1) * A.nilai[k][utama]));
          }
        }
        utama++;
      }
      // Antisipasi hasil -0.0
      for (int i = 0; i < jmlBrs; i++) {
        for (int j = 0; j < jmlKol; j++) {
          A.nilai[i][j]+=0.0;
        }
      }
      return A;
    }


    // penyelesaian SPL Crammer
    public Matriks Crammer(Matriks b) {
      Matriks A = this;
      Matriks Hasil = new Matriks(A.M,1);
      Matriks Clone = new Matriks(A.M, A.N);
      Double DeterA;
      Clone = Clone.Copy(A);
      DeterA=Clone.Determinan();

      if ((DeterA+0.0)==0) {
        Hasil = new Matriks(1,1);
        Hasil.nilai[0][0]=0;
        return Hasil;
      }

      for (int i=0;i<A.N;i++) {
        Clone = Clone.Copy(A);
        Clone = Clone.GantiKolom(b,i);
        Hasil.nilai[i][0]=Clone.Determinan()/DeterA;
      }
      return Hasil;
    }

}
